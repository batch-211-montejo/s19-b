console.log("Hello World");

// What are the conditional Statement
// conditional statelemts allows us to control the flow of the prohram
// it allows us to rn a statemtn.instruction if a condition is met or run another seperate instruction if other wise

// if, else if, and else element

let numA = -1;

// if statement
	// execute a statement if a specified condition is true

	if(numA<0) {
		console.log("Yes! It is less than zero")
	};


	/*
		syntax:


		if(condition) {
			statement
		}
	*/
	// the result of the expression added in the if's condition must result to true, else, the statement inside if () will not run
	console.log(numA<0); //results to true == if the statement will not run

	numA = 0;

	if(numA<0) {
		console.log("Hello");
	}

	console.log(numA<0) // false

	let city = "New York";

	if(city==="New York") {
		console.log("Welcome to New York City!");
	}

	//else if statemetn

	/*
		-execures a statement if previous conditions are false and if the specified condition is true
		-the "else if" clase us optional and can be aadded to capture additional conditions to change the flow of a program
	*/

	let numH = 1;

	if (numA<0) {
		console.log("Hello");
	} else if (numH>0) {
		console.log("World");
	}

	// we were able to run the else if() statemetn afte we evaluated that the if condition was failed

	// if the if() consition was passed and run, we will no longer evaluate the else if() and end the process there

	numA=1;
	if (numA>0) {
		console.log("Hello");
	} else if (numH>0) {
		console.log("World");
	}

	// else if() statement was no longer run because the if statement was able to run. the evaluation of the whole statemetn stops there

	city = "Tokyo";
	if (city==="New York") {
		console.log("Welcome to New York City!");
	} else if (city==="Tokyo") {
		console.log("Welcome to Tokyo, Japan!");
	}

	// since we failed the condition for the first if(), we went to the else if () and checked and instead passed that condition

	// else statememt

		/*
			-ecxeutes a statement if all other conditons are false
			-the "else" stataement is optional and can be added to capture any other result to change the flow of a program
		*/

	if (numA<0) {
		console.log("Hello");
	} else if (numA===0) {
		console.log("World");
	} else {
		console.log("Again");
	}

	/*
		sicne both the proceeding if and else if conditions are not met/failed, the else statement was run instead

		else statement should only be added if there is a proceeding if condtion.
		else statements by itself will not work. However, if statement will work even if there is no else statemen

	*/

	// else {
	// 	console.log("Will not run without an if");
	// }

	/*else if (numH===0) {
		console.log("World");
	} else {
		console.log("Again");
	}*/

	// same goes for an else if, there should be a proceeding if() first

	// if, else if, and else statement with functions

	/*
		-most of the times, we would like to use if, else if and else statement with functions to control the flow of our application
	*/

	let message = "No message.";
	console.log(message);

	function determinedTyphoonIntensity(windSpeed) {

		if(windSpeed < 30) {
			return "Not a Typhoon yet.";
		} else if (windSpeed<=61) {
			return "Tropical depression detected";
		} else if (windSpeed >= 62 && windSpeed <= 88) {
			return "Tropical Storm detected";
		} else if (windSpeed >= 89 || windSpeed <= 117) {
			return "Severe stropical storm detected.";
		} else {
			return "Typhone detected";
		}
	}
	// returns the string to the variable "message" that invoked it
	message = determinedTyphoonIntensity(110);
	console.log(message);

	if (message == "Severe stropical storm detected.") {
		console.warn(message);
	}
	// console.warn() is a good wait to print warnings in our console that could help is developers act on certain output without code


	// mini activity #1 :)
	// function oddOrEvenChecker(num1) {
	// 	if(num1%2===0) {
	// 		return "Even"
	// 	} else if (num1%2>0) {
	// 		return "Odd";
	// 	}
	// }

	// let check = oddOrEvenChecker(2);
	// 	console.log("check 2");
	// 	alert(check);

	// solution of ms. cee

	// function oddOrEvenChecker(num) {
	// 	if ( num%2 === 0) {
	// 		alert (num)
	// 	}
	// }

	// function checkAge(age) {
	// 	if (age<=17) {
	// 		return "not allowed to drink"
	// 	} else if (age >=18) {
	// 		return "allowed to drink"
	// 	}
	// }

	// let isAllowedToDrink = checkAge(18);
	// console.log("Is 18 allowed to drink:");
	// alert(isAllowedToDrink>=18);

	// minit ativity 2
	// function ageChecker (num) {
	// 	if (num<17) {
	// 		alert (num + " is underaged");
	// 		return(false);
	// 	} else {
	// 		alert(num + " is allowed to drink");
	// 		return true;
	// 	}
	// }

	// let isAllowedToDrink = ageChecker(19);
	// console.log(isAllowedToDrink);

	// Truthy and Falsy

	/*
		-in JS a "truthy" value is a value taht si considered true when encountered in Boolean contect
		-Values in are considered trued unless defined otherwise
		-Falsy Values/esceptions for truth:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN
	*/

	// Truthy Examples
	/*
		-if the result of an expression in a condition reasults to a truthy value, the condition eturns and the corresponding statement are executed
		-Expressions are any unit of code that can be evaluated to a value
	*/

	if (true) {
		console.log("Truthy");
	}
	if (1) {
		console.log("Truthy");
	}
	if ([1,2,3]) {
		console.log("Truthy");
	}

	// Falsy examples

	if (false) {
		console.log("Falsy");
	}
	if (0) {
		console.log("Falsy");

	}
	if (undefined) {
		console.log("Falsy");

	}


	// Conditional (ternary) operator

	/*
		-the conditional (ternary) operator takes in three operands:
			1. condition
			2. expression to execute if the condition is truthy
			3. expression to execute if the condition is falsy

		-can be used as an alternative to as "if else " statement
		-ternary operators have an implicit "return" statement maning that without the "return" keywork, the resulting expressions can be stored in a variable

		-Syntax

			(expression) ? ifTrue: ifFalse;
	*/

	// single statement execution

	let ternaryResult = (1<18) ? true : false;
	console.log("Result of Ternary Operator: " +ternaryResult);
	console.log(ternaryResult);

	// Multiple statement execution
	/*
		a function may be defines then used in a ternary operator
	*/

	let name;

	function isOfLegalAge () {
		name = "John";
		return "You are of the legal age limit";
	}

	function isUnderAge() {
		name ="Jane";
		return "You are under the age limit";
	}

	let age = parseInt(prompt("What is your age:"));
	console.log(age);
	let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
	console.log("result of ternary operator in functions: " + legalAge + ", " + name);

	// switch stament
	/*
		-The swith statement evaluates an expression and matches the exoression's value to a case clause

		-toLowerCase function will change the input received from the prompt into all lowercase letters ensuring a match with the switch case
		-The "expression" is the information used to match the "value" provided in the switch cases 
		=Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
		-break statement is used to terminate the current logo once a match has been found
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch(day) {
		case "monday" :
		console.log("The color of the day is red");
		break;
		case "tuesday" :
		console.log("The color of the day is orange");
		break;
		case "wednesday" :
		console.log("The color of the day is yellow");
		break;
		case "thursday" :
		console.log("The color of the day is green");
		break;
		case "friday" :
		console.log("The color of the day is blue");
		break;
		case "saturday" :
		console.log("The color of the day is indigo");
		break;
		case "sunday" :
		console.log("The color of the day is violet");
		break;
		default:
		console.log("Please input a valid day");
	}


	// mini activity

	// function bear() {
	// let bear = prompt("What is the name of the bear?")

	// 	switch (bear) {
	// 		case 1 : 
	// 		alert("Hi, Im amy!");
	// 		break;
	// 		case 2 : 
	// 		alert("Hi, Im lulu!");
	// 		break;
	// 		case 3 : 
	// 		alert("Hi, Im morgan!");
	// 		break;
	// 		default:
	// 		alert(bear + " is out of bounds")
	// 	}
	// }

	// let bearNumber = bear();


	// solution!!!
	function determineBear(bearNumber) {
		let bear;
		switch(bearNumber) {
			case 1 : 
			alert("Hi, Im amy!");
			break;
			case 2 : 
			alert("Hi, Im lulu!");
			break;
			case 3 : 
			alert("Hi, Im morgan!");
			break;
			default:
			bear = bearNumber + " is out of bounds";
		}
		return bear;
	}
	determineBear(2);




	// Try-Catch-Finally Statement
	/*
		-"try-catch statements are commonly used for error handling
		-they are used to specify a response whenever an error is received
	*/

	function showIntensityAlert(windSpeed) {
		try {
			alerat(determinedTyphoonIntensity(windSpeed))
			// error/err are commonly used variable names used bu developers for storing errors
		} catch (error) {
			console.log(typeof error);
			// the error.message is used to access the information relating to an error oboject
			console.warn(error.message)
		} finally {
			// continue execution of vode regardless of success and failure of code execution in the 'try' block to handle/resolve errors
			alert("Intensity updated will show new alert");
		}
	}

	showIntensityAlert(56);

